---
layout: home
---

# Welcome to the ACC CS/IT Tutoring Portal

<div id="tutoring-available"> &nbsp; </div>

If there is a tutor available, a chat box will pop up to the right. To begin,
enter your information. The next available tutor in your subject will be right
with you.

To use this service, you must provide your __ACC email address__. A tutor will
not be able to help you if you do not provide a valid ACC email address. A Webcam
and microphone is encouraged but not required to use this service. To connect for
more advanced problems, a tutor will ask to start a google hangout via your ACC
email address to enable screensharing.

## Tutoring Schedule

<iframe src="https://calendar.google.com/calendar/b/2/embed?height=600&amp;wkst=2&amp;bgcolor=%238E24AA&amp;ctz=America%2FChicago&amp;src=Zy5hdXN0aW5jYy5lZHVfdjNtdWJqOW9iMmt1dnRvMjRqamZ0Nmk0bmNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23F4511E&amp;mode=WEEK&amp;showTitle=0&amp;showNav=0&amp;showTabs=0&amp;showPrint=0&amp;showDate=0&amp;showCalendars=0" style="border-width:0" width="1024" height="600" frameborder="0" scrolling="no"></iframe>

